package com.kancy.okhttp.client.log.config;

import com.kancy.okhttp.client.log.HttpRequestLogRepositoryImpl;
import com.kancy.okhttp.client.log.properties.OkHttpClientLogProperties;
import com.kancy.okhttp.client.log.repository.HttpRequestLogMongoRepository;
import com.kancy.okhttp.client.support.ApplicationContextHolder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

/**
 * HttpClientLogAutoConfiguration
 *
 * @author kancy
 * @date 2020/5/2 13:41
 */
@EnableConfigurationProperties(OkHttpClientLogProperties.class)
@Import({ApplicationContextHolder.class, HttpRequestLogRepositoryImpl.class, HttpRequestLogMongoRepository.class})
public class HttpClientLogAutoConfiguration {

}
