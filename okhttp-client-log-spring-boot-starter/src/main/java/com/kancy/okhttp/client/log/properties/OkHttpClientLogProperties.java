package com.kancy.okhttp.client.log.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * OkHttpClientLogProperties
 *
 * @author kancy
 * @date 2020/5/2 12:25
 */
@ConfigurationProperties("okhttp.client.log")
public class OkHttpClientLogProperties {

    /**
     * 是否开启该功能
     */
    private boolean enabled = true;

    /**
     * MongoRepository 配置
     */
    private Mongo mongo = new Mongo();

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Mongo getMongo() {
        return mongo;
    }

    public void setMongo(Mongo mongo) {
        this.mongo = mongo;
    }

    public static class Mongo {
        /**
         * 是否开启
         */
        private boolean enabled = true;
        /**
         * MongoTemplate Bean的名称
         * 只有当前环境中存在多个MongoTemplate时生效
         */
        private String mongoTemplateBeanName = "mongoTemplate";
        /**
         * 集合名称
         */
        private String collectionName = "http_request_log";

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        public String getMongoTemplateBeanName() {
            return mongoTemplateBeanName;
        }

        public void setMongoTemplateBeanName(String mongoTemplateBeanName) {
            this.mongoTemplateBeanName = mongoTemplateBeanName;
        }

        public String getCollectionName() {
            return collectionName;
        }

        public void setCollectionName(String collectionName) {
            this.collectionName = collectionName;
        }
    }
}
