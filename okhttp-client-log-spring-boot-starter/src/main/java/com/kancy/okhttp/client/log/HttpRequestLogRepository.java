package com.kancy.okhttp.client.log;

/**
 * HttpRequestLogRepository
 *
 * @author kancy
 * @date 2020/5/2 12:06
 */
public interface HttpRequestLogRepository {
    /**
     * 保存第三方日志
     * @param httpRequestLog 日志对象
     */
    void save(HttpRequestLog httpRequestLog);
}
