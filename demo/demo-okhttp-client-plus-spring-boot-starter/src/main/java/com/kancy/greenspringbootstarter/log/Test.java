package com.kancy.greenspringbootstarter.log;

import com.kancy.okhttp.client.log.HttpRequestLog;
import com.kancy.okhttp.client.log.HttpRequestLogRepository;
import org.springframework.stereotype.Component;

/**
 * Test
 *
 * @author kancy
 * @date 2020/5/2 13:46
 */
@Component
public class Test implements HttpRequestLogRepository {
    /**
     * 保存第三方日志
     *
     * @param httpRequestLog 日志对象
     */
    @Override
    public void save(HttpRequestLog httpRequestLog) {
        System.out.println(httpRequestLog);
    }
}
