package com.kancy.greenspringbootstarter.controller;

import com.kancy.okhttp.client.service.HttpClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * DemoController
 * <p>
 *
 * @author: kancy
 * @date: 2020/4/29 16:06
 **/
@Slf4j
@RequiredArgsConstructor
@RestController
public class DemoController {
    private final HttpClient httpClient;

    @GetMapping("/demo")
    public String demo(){
        try {
            TimeUnit.SECONDS.sleep(2);
            log.info("成功！");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "ok";
    }

    @GetMapping("/demo/forward")
    public String forward(String timeout){
        log.warn("警告！");
        return httpClient.sendGet("http://localhost:8080/demo?timeout=%s", String.class, timeout);
    }

    @ExceptionHandler(value = {Exception.class})
    public String fail(){
        return "fail";
    }
}
