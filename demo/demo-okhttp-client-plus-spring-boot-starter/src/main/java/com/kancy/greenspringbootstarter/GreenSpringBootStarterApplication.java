package com.kancy.greenspringbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreenSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(GreenSpringBootStarterApplication.class, args);
    }

}
