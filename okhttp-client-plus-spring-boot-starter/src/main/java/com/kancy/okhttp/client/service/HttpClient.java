package com.kancy.okhttp.client.service;

/**
 * HttpClient
 *
 * @author kancy
 * @date 2020/5/1 15:15
 */
public interface HttpClient {

    /**
     * 发送GET请求
     * @param url
     *  请求地址：http://ip:port/url/%s/%s?param1=%d&param2=%s
     * @param params 参数
     * @return
     */
    String sendGet(String url, Object ... params);
    /**
     * 发送GET请求
     * @param url 地址
     * @param responseClass 返回类型
     * @return
     */
    <T> T sendGet(String url, Class<T> responseClass, Object ... params);

    /**
     * 发送GET请求
     * @param url 地址
     * @param header 请求头
     * @param responseClass 返回类型
     * @param params url变量参数
     * @return
     */
    <T> T sendGet(String url, Object header, Class<T> responseClass, Object ... params);

    /**
     * 发送POST请求
     * @param url
     * @param params url变量参数
     * @return
     */
    String sendPost(String url, Object ... params);

    /**
     * 发送POST请求
     * @param url 地址
     * @param jsonBody 请求体
     * @param params url变量参数
     * @return
     */
    String sendPost(String url, Object jsonBody, Object ... params);

    /**
     * 发送POST请求
     *
     * @param url      地址
     * @param jsonBody 请求体
     * @param responseClass 返回类型
     * @param params url变量参数
     * @return
     */
    <T> T sendPost(String url, Object jsonBody, Class<T> responseClass, Object ... params);

    /**
     * 发送POST请求
     * @param url 地址
     * @param jsonBody 请求体
     * @param header 请求头
     * @param responseClass 返回类型
     * @param params url变量参数
     * @return
     */
    <T> T sendPost(String url, Object jsonBody, Object header, Class<T> responseClass, Object ... params);
}
