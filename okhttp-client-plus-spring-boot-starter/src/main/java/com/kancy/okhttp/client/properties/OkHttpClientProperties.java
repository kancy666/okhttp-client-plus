package com.kancy.okhttp.client.properties;

import java.time.Duration;

/**
 * OkHttpClientProperties
 *
 * @author kancy
 * @date 2020/5/1 13:06
 */
public class OkHttpClientProperties {
    /**
     * 日志拦截能力
     */
    private boolean logInterceptorEnabled = true;
    /**
     * 最大空闲连接
     */
    private int maxIdleConnections = 50;
    /**
     * 保持活跃时间
     */
    private Duration keepAlive = Duration.ofMinutes(5);

    /**
     * 连接超时时间
     */
    private Duration connectTimeout = Duration.ofSeconds(5);
    /**
     * 读超时时间
     */
    private Duration readTimeout = Duration.ofSeconds(10);
    /**
     * 写超时时间
     */
    private Duration writeTimeout = Duration.ofSeconds(10);

    public boolean isLogInterceptorEnabled() {
        return logInterceptorEnabled;
    }

    public void setLogInterceptorEnabled(boolean logInterceptorEnabled) {
        this.logInterceptorEnabled = logInterceptorEnabled;
    }

    public int getMaxIdleConnections() {
        return maxIdleConnections;
    }

    public void setMaxIdleConnections(int maxIdleConnections) {
        this.maxIdleConnections = maxIdleConnections;
    }

    public Duration getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(Duration keepAlive) {
        this.keepAlive = keepAlive;
    }

    public Duration getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Duration connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public Duration getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Duration readTimeout) {
        this.readTimeout = readTimeout;
    }

    public Duration getWriteTimeout() {
        return writeTimeout;
    }

    public void setWriteTimeout(Duration writeTimeout) {
        this.writeTimeout = writeTimeout;
    }
}
