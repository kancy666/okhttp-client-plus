## 简介
一款基于OkHttp封装的增强版Http Client工具

## 主要功能

### 1. 动态设置超时时间

- 方式一：添加QueryParams

    - timeout,connectTimeout,readTimeout,writeTimeout
    - 单位秒

- 方式二：添加Http Header

    - timeout,connectTimeout,readTimeout,writeTimeout
    - 单位秒

### 2. 日志拦截器
- 集成Sleuth
- 第三方请求日志无缝保存到Mongodb 
    - 过期索引: `db.http_request_log.createIndex( { "createTime": 1 }, { expireAfterSeconds: 2592000 } )`
### 3. 集成Micrometer监控

### 4. 内置HttpClient工具

- 内置HttpClient服务
- 内置RestTemplate
- 内置HttpSender 